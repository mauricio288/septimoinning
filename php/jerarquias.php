<?php


 	function getOffset($url){

 		$offset = 0;

		for($i=0; $url; $i++)
			if($url[$i] == 'P' && $url[$i+1] == 'a' && $url[$i+2] == 'n' && $url[$i+3] == 'e' && $url[$i+4] == 'l' && $url[$i+5] == '/' ){
		    	$offset = $i+6;
		      	break;
		    }

		return $offset;  
 	}

 	function nombrePagina($url){
 		$url2 = "";

 		$offset = getOffset($url);

  		for($j = $offset; $j < strlen($url) ; $j++)
    		$url2 = $url2 . $url[$j];

    	return $url2;
 	}

 	function access($perfil, $url){

 		$page = nombrePagina($url);

 		switch ($page) {
 			case 'perfil.php':
 				if($perfil == 3)
 					header("Location: ../Panel/publicar.php");
 				break;
 			case 'noticias.php':
 				if( ! ($perfil == 1 || $perfil == 2) ){
 					if($perfil == 3)
 							header("Location: ../Panel/publicar.php");
 				}
 				break;
 			case 'usuarios.php':

 				if($perfil != 1){
 					if($perfil == 2)
 						header("Location: ../Panel/noticias.php");
 					else
 						if($perfil == 3)
 							header("Location: ../Panel/publicar.php");
 				}
 				break;
 			case 'index.php' :
 				if($perfil != 1){
 					if($perfil == 2)
 						header("Location: ../Panel/noticias.php");
 					else
 						if($perfil == 3)
 							header("Location: ../Panel/publicar.php");
 				}
 				break;
 			case '' :
 				if($perfil != 1){
 					if($perfil == 2)
 						header("Location: ../Panel/noticias.php");
 					else
 						if($perfil == 3)
 							header("Location: ../Panel/publicar.php");
 				}
 				break;
 			case 'banners.php':
 				if($perfil != 1){
 					if($perfil == 2)
 						header("Location: ../Panel/noticias.php");
 					else
 						if($perfil == 3)
 							header("Location: ../Panel/publicar.php");
 				}
 				break;
 		}
 	}

?>