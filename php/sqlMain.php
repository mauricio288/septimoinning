<?php
	include("sqlFunctions.php");
	include("selects.php");
	include("blob_funciones.php");

	$table = $_GET["table"];

	switch ($_GET["crud"]) {
		case 'c':

			$variables = getPostVariables();

			$res = insertSQL($table, $variables);

			break;
		
		case 'u':

			$variables = getPostVariables();

			$id = $_GET["id"];
			$filtro = "WHERE id = $id";

			$res = updateSQL($table, $variables, $filtro);

			break;

		case 'd':
			$id = $_GET["id"];
			$filtro = "WHERE id = $id";
			deleteSQL($table, $filtro);
			break;

		case 'blobNuevo':
			$id = $_GET["id"];
			if( $_FILES["archivito"]["name"] != ""){
				actualizar_blob($id, $table);
			}
			break;

		case 'registrarBlob':
			$id = $_GET["id"];
			if( $_FILES["archivito"]["name"] != ""){
				insertArchivos($id, $table);
			}
			break;

		default:
			# code...
			break;
	}

	$url = (isset($_POST["url"]))?urldecode($_POST["url"]):'';
	header('Location:'.$url);
?>