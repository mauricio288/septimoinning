<?php
	include("dbconnect.php");

	function getPostVariables(){
		foreach($_POST as $key => $value){
			if($key != 'url'){
				$form_data[$key] = mysqli_real_escape_string($GLOBALS["conn"],filter_var($value, FILTER_SANITIZE_STRING));
			}
		}
		return $form_data;
	}

	function cleanJsonArrayVariables($arreglo){
		foreach($arreglo as $key => $value){
			if($key != 'url' && $key != 'o'){
				$form_data[$key] = mysqli_real_escape_string($GLOBALS["conn"],filter_var($value, FILTER_SANITIZE_STRING));
			}
		}
		return $form_data;
	}

	function debug($data){
		foreach ($data as $key => $value) {
			echo $key . ": " . $data[$key] . "<br>";
		}
	}

	function insertSQL($table_name, $form_data, &$lastid = ""){
		$fields = array_keys($form_data);

		$sql = "INSERT INTO ".$table_name."
		(`".implode('`,`', $fields)."`)
		VALUES('".implode("','",  $form_data)."')";

		//echo $sql;
		
		$store = mysqli_query($GLOBALS["conn"], $sql);
		$lastid = (string)mysqli_insert_id($GLOBALS["conn"]);
		//echo mysqli_insert_id($GLOBALS["conn"]);
		
		return $store;
	}

	function selectSQL($table_name, $filter = ""){
		$qry = "SELECT * FROM ".$table_name." ".$filter;
		$res = mysqli_query($GLOBALS["conn"], $qry);
		
		return $res;
	}

	function updateSQL($table_name, $form_data, $where_clause=''){
		
		$sql = "UPDATE ".$table_name." SET ";

		$sets = array();

		foreach($form_data as $column => $value){
			$sets[] = "`".$column."` = '".$value."'";
		}
		$sql .= implode(', ', $sets);

		$sql .= $where_clause;

		return mysqli_query($GLOBALS["conn"],$sql);
	}

	function deleteSQL($table_name, $where_clause = ""){
		$qry = "DELETE FROM ".$table_name." ".$where_clause;
		$res = mysqli_query($GLOBALS["conn"], $qry);
		
		return $res;
	}
?>