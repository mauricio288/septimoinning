<?php
	require("dbconnect.php");
	require("blob_funciones.php");
	
	function eliminar_tabla($tabla, $comp = ""){
		$qry = "DELETE FROM ".$tabla." ".$comp;
		$res = mysqli_query($GLOBALS["conn"], $qry);
		
		return $res;
	}
	
	$tabla = $_GET["tabla"];
	$id = $_GET["id"];
	
	switch($tabla){	
	
		case "divisiones":
			$goto = "divisiones.php";
			break;
		
		case "publicaciones":
			$goto = "index.php";
			break;

		case "productos":
			$goto = "index.php";
			break;
			
		case "categorias":
			$goto = "lym.php";
			break;
			
		case "u": //usuarios
			$goto = "usuarios.php?t=".$tabla;
			$tabla = "usuarios";
			break;
			
		case "p": //perfiles
			$goto = "usuarios.php?t=".$tabla;
			$tabla = "perfiles";
			break;

		default:
			$goto = $tabla . ".php";
			break;
	}

	$filtro = "WHERE id = ".$id;
	$res = eliminar_tabla($tabla, $filtro);

	if($res){

		if(!eliminar_blob("WHERE idTabla = $id and tablaBD = '$tabla' ", $tabla))
			exit("Falla en eliminar blob: No se pudo eliminar la foto de " . $tabla . " id: ". $idTabla);

		header('Location: '. "../Panel/".$goto);
	}
	else{
	
		if($tabla == "categorias")
			print "NOOOOOOO se ha podido eliminar la tabla debido a que existen servicios que dependen de esta categoria";
		else
			print "NOOOOOOO se ha eliminado el registro en la tabla: ".$tabla;
	}
?>