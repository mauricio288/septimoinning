<?php
	
	function selectSQL_oo($table, $filter, &$arr, $conection){

		if($table != "archivos"){
			$qry = "SELECT * FROM  " . $table.$filter ;
		}
		else{
			$qry = "SELECT id FROM archivos " . $filter ;
		}

		$res = $conection->query($qry) or die($conection->error." line ".__LINE__);
		if($res->num_rows > 0) {
			while($row = $res->fetch_assoc()) {
				$arr[] = $row;	
			}
		}
	}

	function insertSQL_oo($table_name, $form_data, $conection, &$lastid = ""){
		$fields = array_keys($form_data);

		$qry = "INSERT INTO ".$table_name."
		(`".implode('`,`', $fields)."`)
		VALUES('".implode("','",  $form_data)."')";

		
		$res = $conection->query($qry)	or die($conection->error." line ".__LINE__);	
		$lastid = (string) $conection->insert_id;
		
		return $res;
	}

	function deleteSQL_oo($table, $filter , $conection){
		$qry = "DELETE FROM ".$table." ".$filter;
		$res = $conection->query($qry) or die($conection->error." line ".__LINE__);

		return $res;
	}

	function updateSQL_oo($table_name, $form_data, $conection ,$where_clause=''){
		
		$qry = "UPDATE ".$table_name." SET ";

		$sets = array();

		foreach($form_data as $column => $value){
			$sets[] = "`".$column."` = '".$value."'";
		}
		$qry .= implode(', ', $sets);

		$qry .= $where_clause;

		$res = $conection->query($qry) or die($conection->error." line ".__LINE__);

		return $res;
	}

?>