<?php
	
	include("../dbVariables.php");
	
	$GLOBALS["conn"] = mysqli_connect(_HOST_NAME, _DATABASE_USER_NAME, _DATABASE_PASSWORD, _DATABASE_NAME);
	// Check connection
	if (mysqli_connect_errno()){
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

	mysqli_set_charset($GLOBALS["conn"],"utf8");
	
?>