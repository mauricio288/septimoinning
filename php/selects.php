<?php
	require("dbconnect.php");

	function stringDate($date){
		$string = $date[8] . $date[9] . " de " . monthFromNumberToString($date[5].$date[6]) . " de " . $date[0] . $date[1] . $date[2] . $date[3];

		return $string;
	}

	function monthFromNumberToString($month){

		switch ($month) {
			case '01':
				$month = "Enero";
				break;
			case '02':
				$month = "Febrero";
				break;
			case '03':
				$month = "Marzo";
				break;
			case '04':
				$month = "Abril";
				break;
			case '05':
				$month = "Mayo";
				break;
			case '06':
				$month = "Junio";
				break;
			case '07':
				$month = "Julio";
				break;
			case '08':
				$month = "Agosto";
				break;
			case '09':
				$month = "Septiembre";
				break;
			case '10':
				$month = "Octubre";
				break;
			case '11':
				$month = "Noviembre";
				break;
			case '12':
				$month = "Diciembre";
				break;
		}
		return $month;
	}

	function resultados_consultar_tabla($tabla, $filtro = ""){
		$qry = "SELECT * FROM ".$tabla." ".$filtro;
		$res = mysqli_query($GLOBALS["conn"], $qry);
		
		return $res;
	}
	
	function cualUsuario($id){
		
		$res = mysqli_query($GLOBALS["conn"], "SELECT nombre FROM usuarios WHERE id = $id");
		
		$row = mysqli_fetch_row($res);
		
		return $row['0'];
	
	}
	
	function cualPerfil($id){
		
		$res = mysqli_query($GLOBALS["conn"], "SELECT tipo FROM perfiles WHERE id = $id");
		
		$row = mysqli_fetch_row($res);
		
		return $row['0'];
	
	}
	
	function cualDivision($id){
		
		$res = mysqli_query($GLOBALS["conn"], "SELECT nombre FROM divisiones WHERE id = $id");
		
		$row = mysqli_fetch_row($res);
		
		return $row['0'];
	
	}
	
	function cualMarca($id){
		
		$res = mysqli_query($GLOBALS["conn"], "SELECT nombre FROM marcas WHERE id = $id");
		
		$row = mysqli_fetch_row($res);
		
		return $row['0'];
	
	}
	
	function cualLinea($id){
		
		$res = mysqli_query($GLOBALS["conn"], "SELECT nombre FROM lineas WHERE id = $id");
		
		$row = mysqli_fetch_row($res);
		
		return $row['0'];
	
	}

	function queQuieroDeEsteID($quiero, $id, $tabla){
		
		$res = mysqli_query($GLOBALS["conn"], "SELECT ".$quiero." FROM ".$tabla." WHERE id = $id");
		
		if($res){
			$row = mysqli_fetch_row($res);
		
			return $row['0'];
		}
		else{
			return "";
		}
		
	
	}
	
	function imagenMarca($id){
		$res = mysqli_query($GLOBALS["conn"], "SELECT imagen FROM marcas WHERE id = $id");
		
		$row = mysqli_fetch_row($res);
		
		return $row['0'];
	}

	function imagenLinea($id){
		$res = mysqli_query($GLOBALS["conn"], "SELECT imagen FROM lineas WHERE id = $id");
		
		$row = mysqli_fetch_row($res);
		
		return $row['0'];
	}
	
	function aQueMarcaPerteneceLinea($id){
		$res = mysqli_query($GLOBALS["conn"], "SELECT idMarca FROM lineas WHERE id = $id");
		$row = mysqli_fetch_row($res);
		return $row['0'];
	}
	
	function aQueDivisionPerteneceMarca($id){
		$res = mysqli_query($GLOBALS["conn"], "SELECT idDivision FROM marcas WHERE id = $id");
		$row = mysqli_fetch_row($res);
		return $row['0'];
	}
	
?>