<?php 

	include("../php/dbconnect-oo.php");
	include("../php/sqlFunctions-oo.php");
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");

	//Obtención del tipo del operación REST
	$method = $_GET["method"];

	if($method == "get"){

		//Obtener tabla
		$table = $_GET["table"];

		$filter = "";
		//Ordenar por el más reciente
		if($table != "cosas")
			$filter = " ORDER BY id DESC ";

		//Paginación si es que se solicito
		if(isset($_GET["limit"]) && isset($_GET["offset"]) ){
			$filter = " LIMIT " . $_GET["limit"] . " OFFSET ". $_GET["offset"] ;
		}

		//Crear el arreglo de resultados
		$arr = array();
		//Llenarlo con el qry correspondiente
		selectSQL_oo($table, $filter, $arr, $mysqli);

		//Enviarlo
		echo json_encode($arr);

		//Fin del Rest
		exit(1);
	}

	if($method == "getBlobs"){

		$table = "archivos";
		$tabla = $_GET["tablaBD"];
		$id = $_GET["idTabla"];

		//Ordenar por el más reciente
		$filter = " WHERE idTabla = $id and tablaBD = '$tabla' ";

		//Crear el arreglo de resultados
		$arr = array();
		//Llenarlo con el qry correspondiente
		selectSQL_oo($table, $filter, $arr, $mysqli);

		//Enviarlo
		echo json_encode($arr);

		//Fin del Rest
		exit(1);
	}

	//Recibe uno o varios objetos
	if($method == "delete"){
		//Tabla solicitada
		$table = $_GET["table"];
		//Bandera para saber si se borrarán blobs en la tabla archivos
		$blob = $_GET["blob"];
		//Obtener el contenido del POST
		$postdata = file_get_contents("php://input");
		//Acomodar el contenido del post en un arreglo
		$arreglo = json_decode($postdata);

		//Variable acumulativa para el mensaje 
		$mensaje = "";
		foreach($arreglo as $obj){
			//Acumulación de ID
			$mensaje = $mensaje . $obj->id ;

			//Entrar a borrar los blobs
			deleteSQL_oo("archivos", " WHERE idTabla = ". $obj->id ." and tablaBD = '$table' " , $mysqli);
			

			//Creación del qry
			$filter = " where id = " . $obj->id . " " ;
			//Borrar en la BD
			deleteSQL_oo($table, $filter , $mysqli);
		}

		echo json_encode($mensaje);

	}

	//Recibe solo un objeto
	if($method == "post"){
		$table = $_GET["table"];
		$postdata = file_get_contents("php://input");
		$arreglo = json_decode($postdata, true);
		//echo json_encode($arreglo);

		$id = "0";

		insertSQL_oo($table, $arreglo, $mysqli, $id);

		echo json_encode($id);
	}

	//Recibe varios objetos
	if($method == "postObjects"){
		$table = $_GET["table"];
		$postdata = file_get_contents("php://input");
		$arreglo = json_decode($postdata);

		$mensaje = "";
		foreach($arreglo as $obj){
			$mensaje = $mensaje . $obj->id . " ";
		}

		echo json_encode($mensaje);

	}

	//Recibe solo un objeto
	if($method == "update"){
		$table = $_GET["table"];
		$postdata = file_get_contents("php://input");
		$arreglo = json_decode($postdata, true);

		updateSQL_oo($table, $arreglo, $mysqli , "where id = " . $arreglo["id"]  );

		//echo json_encode($arreglo);
	}
?>