<?php
	require("dbconnect.php");
	
	function insertArchivos($idTabla, $tablaBD){

		$archivo = $_FILES["archivito"]["tmp_name"]; 
		$tamanio = $_FILES["archivito"]["size"];
		$tipo    = $_FILES["archivito"]["type"];
		$nombre  = $_FILES["archivito"]["name"];

		if ( $archivo != "none" ){
			$fp = fopen($archivo, "rb");
			$contenido = fread($fp, $tamanio);
			$contenido = addslashes($contenido);
			fclose($fp); 

			$qry = "INSERT INTO archivos (nombre, contenido, tipo, idTabla, tablaBD) VALUES
					('$nombre','$contenido','$tipo' , $idTabla, '$tablaBD')";

			return mysqli_query($GLOBALS["conn"],$qry);

		}
		else{
			print "No se ha podido subir el archivo al servidor";
			exit(1);
		}
			
	}

	function eliminar_blob( $comp = "", $tabla){

			$qry = "DELETE FROM archivos ".$comp;
			$res = mysqli_query($GLOBALS["conn"], $qry);
			
			return $res;
		
	}

	function actualizar_blob($idTabla, $tabla){

		if(!eliminar_blob("WHERE idTabla = $idTabla and tablaBD = '$tabla' ", $tabla))
			exit("Falla en actualizar blob: No se pudo eliminar la foto de " . $tabla . " id: ". $idTabla);

		if(! insertArchivos($idTabla, $tabla) )
			exit("Falla en actualizar blob: No se pudo insertar la foto de " . $tabla . " id: ". $idTabla);
	}

?>