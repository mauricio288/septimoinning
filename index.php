<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Septimoinning</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script> -->
        
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css"> 
        <link rel="stylesheet" href="assets/css/custom.css">

        <script src="https://use.fontawesome.com/e640964938.js"></script>
    </head>
    <body id="ReadApp">

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8&appId=1069747949751584";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include 'header.html'; ?>

        <section ng-controller="indexCtrl">
            <div class="container">
                <div class="news-section">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3 class=" black-title black-title-top ">Lo mas nuevo </h3>
                            <div class="news-content" ng-repeat="n in noticiasArray" ng-if="$index < 1">                                
                                <br>

                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                  <!-- Indicators -->
                                  <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                    <li data-target="#myCarousel" data-slide-to="2"></li>
                                  </ol>

                                  <!-- Wrapper for slides -->
                                  <div class="carousel-inner" role="listbox">

                                    <div class="item active">
                                        <a ng-repeat="c in noticiasArray | limitTo:4 | orderBy:fecha "  href="news-single.php?n={{c.id}}&c={{c.idCategoria}}">
                                            <img ng-if = "$index == 1" class="img-responsive" src="php/visualizar_archivo.php?tabla=noticias&id={{c.id}}" alt="" >
                                        </a>
                                        <br>
                                        <a ng-repeat="c in noticiasArray | limitTo:4 | orderBy:fecha "  href="news-single.php?n={{c.id}}&c={{c.idCategoria}}">
                                            <div class="newest-title" align="center" ng-if = "$index == 1">
                                                <h2>{{c.nombre}}</h2>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="item">
                                        <a ng-repeat="c in noticiasArray | limitTo:4 | orderBy:fecha "  href="news-single.php?n={{c.id}}&c={{c.idCategoria}}">
                                            <img ng-if = "$index == 2" class="img-responsive" src="php/visualizar_archivo.php?tabla=noticias&id={{c.id}}" alt="" >
                                        </a>
                                        <a ng-repeat="c in noticiasArray | limitTo:4 | orderBy:fecha "  href="news-single.php?n={{c.id}}&c={{c.idCategoria}}">
                                            <div class="newest-title" align="center" ng-if = "$index == 2">
                                                <h2>{{c.nombre}}</h2>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="item">
                                        <a ng-repeat="c in noticiasArray | limitTo:4 | orderBy:fecha "  href="news-single.php?n={{c.id}}&c={{c.idCategoria}}">
                                            <img ng-if = "$index == 3" class="img-responsive" src="php/visualizar_archivo.php?tabla=noticias&id={{c.id}}" alt="" >
                                        </a>
                                        <a ng-repeat="c in noticiasArray | limitTo:4 | orderBy:fecha "  href="news-single.php?n={{c.id}}&c={{c.idCategoria}}">
                                            <div class="newest-title" align="center" ng-if = "$index == 3">
                                                <h2>{{c.nombre}}</h2>
                                            </div>
                                        </a>
                                    </div>

                                    
                                  </div>

                                  <!-- Left and right controls -->
                                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Anterior</span>
                                  </a>
                                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Siguiente</span>
                                  </a>
                                </div>
                                
                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3 class="white-title">Video mas nuevo</h3>
                            <div class="news-content">
                                <div ng-repeat="v in videos | limitTo: 1">
                                    <div class="embed-responsive embed-responsive-4by3">
                                      <iframe class="embed-responsive-item" src="{{youtubeUrl(v.id.videoId)}}" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{publicidad.url}}" target="_blank">
                             <img class="img-responsive" src="php/visualizar_archivo.php?tabla=publicidad&id={{publicidad.id}}" style="width:1140px !important; height:90px !important;" alt="{{publicidad.name}}"> 
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <h3 class=" black-title">LMB</h3>
                            <div class="news-content">
                                <div ng-repeat="n in noticiasArray | filter: lmb" ng-if="$index < 2">
                                    <h4><a href="news-single.php?n={{n.id}}&c={{n.idCategoria}}"><strong>{{n.nombre}}</strong></a></h4>
                                    <div ng-bind-html="acortarContenido(n.contenido)"></div>
                                </div>

                                <div class="news-link">
                                    <a href="news.php?c=14">Más Noticias</a>
                                </div>
                            </div>                   
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <h3 class="white-title">LMP</h3>
                            <div class="news-content">
                                <div ng-repeat="n in noticiasArray | filter: lmp" ng-if="$index < 2">
                                    <h4><a href="news-single.php?n={{n.id}}&c={{n.idCategoria}}"><strong>{{n.nombre}}</strong></a></h4>
                                    <div ng-bind-html="acortarContenido(n.contenido)"></div>
                                </div>
                                
                                <div class="news-link">
                                    <a href="news.php?c=13">Más Noticias</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4" align="center" ng-repeat="n in columnas | limitTo:3">
                            <img class="img-responsive column-photo" src="php/visualizar_archivo.php?tabla=noticias&id={{n.id}}" alt="" >
                            <h3>{{n.nombre}}</h3>
                            <br>
                            <a href="news-single.php?n={{n.id}}"><input class="yellow-btn" type="button" value="Leer más"></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 no-padding">
                            <div class="positions">
                                <h3>POSICIONES</h3>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="leaders">
                                <h3>LIDERES</h3>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="diamond-girls">
                                <h3>LAS CHICAS DEL DIAMANTE</h3>
                            </div>
                           <div class="news-content">
                                <div ng-repeat="n in noticiasArray | filter: chicas" ng-if="$index < 2">
                                    <h4><a href="news-single.php?n={{n.id}}&c={{n.idCategoria}}"><strong>{{n.nombre}}</strong></a></h4>
                                    <div ng-bind-html="acortarContenido(n.contenido)"></div>
                                </div>
                                
                                <div class="news-link">
                                    <a href="news.php?c=6">Más Noticias</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{publicidad.url}}" target="_blank">
                             <img class="img-responsive" src="php/visualizar_archivo.php?tabla=publicidad&id={{publicidad.id}}" style="width:1140px !important; height:90px !important;" alt="{{publicidad.name}}"> 
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <a class="twitter-timeline" href="https://twitter.com/septimoinning" data-height="500">Tweets by septimoinning</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                            
                        </div>
                        <div class="col-md-4">
                            <div class="fb-page" data-href="https://www.facebook.com/Septimoinning-1021038211357528/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Septimoinning-1021038211357528/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Septimoinning-1021038211357528/">Septimoinning</a></blockquote></div>
                        </div>
                        <div class="col-md-4">
                            <center>
                                <img src="http://diylogodesigns.com/blog/wp-content/uploads/2016/05/youtube-high-resolution-logo-download.png" class="img-responsive" style="width: 45%">
                            </center>
                            <div class="row" >
                                <div ng-repeat="v in videos | limitTo: 3">
                                    <div clas="col-md-4">
                                        <img src="{{v.snippet.thumbnails.default.url}}" class="img-responsive">
                                    </div>
                                    <div class="col-md-8">
                                        <h5><a href="https://www.youtube.com/watch?v={{v.id.videoId}}">{{v.snippet.title}}</a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
        </section>

        <?php include 'footer.html'; ?>

        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <!-- <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script> -->
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
        <script type="text/javascript" src="js/angular.min.js"></script>
        <script src='js/rich_text/textAngular-rangy.min.js'></script>
        <script src='js/rich_text/textAngular-sanitize.min.js'></script>
        <script src='js/rich_text/textAngular.min.js'></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>