<?php
    $n = $_GET["n"];
    $c = "";
    if(isset($_GET["c"]))
        $c = $_GET["c"];
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Septimoinning</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script> -->
        
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css"> 
        <link rel="stylesheet" href="assets/css/custom.css">

        <script src="https://use.fontawesome.com/e640964938.js"></script>
    </head>
    <body id="ReadApp">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php include 'header.html'; ?>

        <div class="container" ng-controller="singleCtrl">
            <div class="news-section" ng-repeat="n in newsArray | filter:newGet">
                <div class="row">
                    <div class="col-md-12 mobile-title">
                        <h3 class="info-title">{{n.nombre}}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="news-image" align="center">
                            <img ng-if="!n.idArchivo || n.idArchivo==0" class="img-responsive" src="php/visualizar_archivo.php?tabla=noticias&id={{n.id}}" alt="" >
                            <img ng-if="n.idArchivo" class="img-responsive" src="php/visualizar_archivo.php?id={{n.idArchivo}}" alt="" >
                        </div>
                        
                        <div ng-bind-html="n.contenido"></div>

                        <div class="row" style="display:flex; flex-wrap: wrap;">

                            <div class = "col-md-4" ng-repeat = "i in idsBlobs" >
                                <div class="news-image" align="center">
                                    <img src="php/visualizar_archivo.php?id={{i.id}}" class = "img-responsive img-rounded" >
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 spot-horizontal">
                                <h3 class="info-title"><a style="color:#fff;" href="news.php?c={{cat.id}}">Otras noticias</a></h3>
                            </div>
                            <a ng-repeat="c in newsCategoriesArray | limitTo:3 | orderBy:fecha "  href="news-single.php?n={{c.id}}&c={{cat.id}}">
                                <div class="col-md-4 info-container">
                                    <img ng-if="!c.idArchivo || c.idArchivo==0" class="img-responsive" src="php/visualizar_archivo.php?tabla=noticias&id={{c.id}}" alt="" >
                                    <img ng-if="c.idArchivo" class="img-responsive" src="php/visualizar_archivo.php?id={{c.idArchivo}}" alt="" >
                                    <center><h3>{{c.nombre}}</h3></center>
                                </div>
                            </a>
                        </div>
                        <div id="disqus_thread"></div>
                            <script>

                            /**
                            *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                            *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                            /*
                            var disqus_config = function () {
                            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
                            */
                            (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://http-septimoinning-com-web2016.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                            })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                        </div> 
                    </div>        
                </div>
            </div>
        </div>

         <?php include 'footer.html'; ?>

        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <!-- <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script> -->
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
        <script type="text/javascript" src="js/angular.min.js"></script>
        <script src='js/rich_text/textAngular-rangy.min.js'></script>
        <script src='js/rich_text/textAngular-sanitize.min.js'></script>
        <script src='js/rich_text/textAngular.min.js'></script>

        <script type="text/javascript">
            var single = { "id" : <?php echo $n ?> };
        </script>
        <script type="text/javascript">
            var categoria = { "id" : <?php echo $c ?> };
        </script>

        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>