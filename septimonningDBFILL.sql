create schema septimonning;
use septimonning;


CREATE TABLE IF NOT EXISTS `archivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `caracteristica` varchar(100) DEFAULT NULL,
  `tablaBD` varchar(100) DEFAULT NULL,
  `contenido` mediumblob,
  `tipo` varchar(50) DEFAULT NULL,
  `idTabla` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;


CREATE TABLE IF NOT EXISTS `publicidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;


CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;


CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;


INSERT INTO `categorias` (`id`, `nombre`) VALUES
(12, 'Grandes Ligas (MLB)'),
(11, 'Mexicanos en el extranjero'),
(10, 'Las 15 cosas de'),
(9, 'Las chicas del diamante'),
(13, 'Liga Mexicana del Pacífico (LMP)'),
(14, 'Liga Mexicana de Béisbol (LMB)'),
(15, 'Columnas');


CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telefono` varchar(100) DEFAULT NULL,
  `ubicacion` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `acerca` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


INSERT INTO `contacto` (`id`, `telefono`, `ubicacion`, `email`, `acerca`) VALUES
(1, '+52 8180150071', 'Monterrey, N.L.', 'septimoinning@gmail.com', 'Béisbol Profesional Mexicano y Grandes Ligas');


CREATE TABLE IF NOT EXISTS `cosas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `texto` text,
  `idSabias` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idSabias` (`idSabias`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;



CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `autor` varchar(100) DEFAULT NULL,
  `fecha` varchar(10) DEFAULT NULL,
  `hora` varchar(5) DEFAULT NULL,
  `contenido` text,
  `idCategoria` int(11) DEFAULT NULL,
  `idArchivo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCategoria` (`idCategoria`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

CREATE TABLE IF NOT EXISTS 'equipos' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'idCategoria' int(11) NOT NULL,
  'nombre' varchar(100) DEFAULT NULL,
  PRIMARY KEY ('id'),
  KEY 'idCategoria' ('idCategoria'),
);

CREATE TABLE IF NOT EXISTS `perfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


INSERT INTO `perfiles` (`id`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Publicador'),
(3, 'Invitado');



CREATE TABLE IF NOT EXISTS `sabias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `fecha` varchar(10) DEFAULT NULL,
  `hora` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;



CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `contrasena` varchar(30) DEFAULT NULL,
  `idPerfil` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idPerfil` (`idPerfil`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


INSERT INTO `usuarios` (`id`, `nombre`, `email`, `contrasena`, `idPerfil`) VALUES
(3, 'Admin_Test', 'admin_test@gmail.com', 'password', 1),
(2, 'Admin', 'admin@septimoining.com', 'password', 1);

