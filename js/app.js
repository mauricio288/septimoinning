function indexJsonArray(array, id){
  var index = -1;
  for(var i=0; i < array.length; i++){
    if(array[i].id == id){
      index = i;
      break;
    }
  }
  return index;
}

function getFechaActual(){
  var d = new Date();
  var dia = d.getDate();
  var mes = d.getMonth() + 1;
  var ano = d.getFullYear();
  if(mes < 10)
    mes = "0" + mes;
  if(dia < 10)
    dia = "0" + dia;
  var fecha = ano + "-" + mes + "-" + dia;
  return fecha;
}

function getHoraActual(){
  var d = new Date();
  var hora = d.getHours();
  var minuto = d.getMinutes();
  if(hora < 10)
    hora = "0" + hora;
  if(minuto < 10)
    minuto = "0" + minuto;
  var horario = hora + ":" + minuto;

  return horario;
}

function ano(cadena){

  return cadena[0]+cadena[1]+cadena[2]+cadena[3];
}

function mes(cadena){
  var mes = cadena[5] + cadena[6];
  
  switch(mes){
    case "01":
      return "Enero";
    case "02":
      return "Febrero";
    case "03":
      return "Marzo";
    case "04":
      return "Abril";
    case "05":
      return "Mayo";
    case "06":
      return "Junio";
    case "07":
      return "Julio";
    case "08":
      return "Agosto";
    case "09":
      return "Septiembre";  
    case "10":
      return "Octubre";
    case "11":
      return "Noviembre";
    case "12":
      return "Diciembre";
  }

}

function dia(cadena){

  return cadena[8] + cadena[9];
}

var urlREST = "php/mainSQL.php";

var readApp = angular.module('myApp3', ['textAngular']);

readApp.controller('footerCtrl', function($scope, $http, REST, $sce) {

    $scope.suh = "hola mundo";

    $scope.editarContacto = function(contacto){
      alert("Información atualizada exitosamente.")
      contactoAPI.updateData(contacto);
    }

    var contactoAPI = new REST(urlREST, "contacto");

    $scope.contactoArray = [];

    contactoAPI.getElementsOfTable($scope.contactoArray);
});

readApp.controller('headerCtrl', function($scope, $http, REST, $sce) {

  $scope.suh = "hola mundo";

  $scope.categoriasArray = [];
  var categoriasAPI = new REST(urlREST, "categorias");
  categoriasAPI.getElementsOfTable($scope.categoriasArray);

  $scope.bannersArray = [];
  var bannersAPI = new REST(urlREST, "banners");
  bannersAPI.getElementsOfTable($scope.bannersArray);
  // console.log($scope.bannersArray);
});

readApp.controller('indexCtrl', function($scope, $http, REST, $sce, $filter) {

  $scope.videos = [];
      $http.get('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC0oORKSGX7GMhZ86hzlAKag&maxResults=10&order=date&type=video&key=AIzaSyDc8ybBf_0bb1Ov0cJYUpneSjjMk-DQSf8').success(
        function (data) {
          //alert(JSON.stringify(data.items));
          for (var i = 0; i < data.items.length; i++) {
              $scope.videos.push(data.items[i]);
          }
        }
      );

  $scope.youtubeUrl = function(videoId){
    var link = "https://www.youtube.com/embed/" + videoId;
    return $sce.trustAsResourceUrl(link);
  }

  $scope.acortarContenido = function(contenido){
    var preview = "";

    if(contenido.length < 200)
      preview += contenido;
    else{
      for(var i=0; i< 200; i++)
        preview += contenido[i];
      preview += "..."
    }

    return preview;
  }

  $scope.suh = "hola mundo";
  $scope.lmb = { "idCategoria" : "14"};
  $scope.lmp = { "idCategoria" : "13"};
  $scope.chicas = { "idCategoria" : "6"};

  var urlCategorias = urlREST + "?method=get&table=categorias";
  var urlNoticias = urlREST + "?method=get&table=noticias";
  var urlPublicidad = urlREST + "?method=get&table=publicidad";
    
  $http.get(urlPublicidad).success(function (data) {
      $scope.publicidad = data[0];
  });

  $scope.categoriasArray = [];
  $scope.noticiasArray = [];

  $http.get(urlCategorias).success(function (data) {
    for (var i = 0; i < data.length; i++) {
        $scope.categoriasArray.push(data[i]);
    }
      $http.get(urlNoticias).success(function (data) {
          for (var i = 0; i < data.length; i++) {
              $scope.noticiasArray.push(data[i]);
          }
          $scope.columnas = [];
          var myCategoria = $filter('filter')($scope.categoriasArray, {nombre: "Columnas"});
            myCategoria = myCategoria[0]
            angular.forEach($scope.noticiasArray, function(value) {
                if(value.idCategoria == myCategoria.id){
                    $scope.columnas.push(value);
                }
            });
      });
  });
});

readApp.controller('cosasCtrl', function($scope, $http, REST, $sce) {


    $scope.formatoFecha = function(cadena){

      return dia(cadena) + " de " + mes(cadena) + " de " + ano(cadena);
    }

    $scope.nombreSabias = function(id){
      var index = indexJsonArray($scope.sabiasArray, id);

      return $scope.sabiasArray[index].nombre;
    }

    var tablaCtrl = "cosas";
    var CURIOSIDADAPI = new REST(urlREST, tablaCtrl);
    var sabiasAPI = new REST(urlREST, "sabias");
    $scope.idSabias = get.id;
    
    $scope.idCuriosidad = curiosidad.id.toString();
    $scope.random = function() {
        return 0.5 - Math.random();
    }
    $scope.CURIOSIDADArray = [];
    $scope.sabiasArray = [];
    $scope.buscador = {
      "idSabias" : $scope.idSabias
    }

    $scope.asdf= function(c){
      $scope.curiosidadFilter = c;
    }

    $scope.strict=true;

    CURIOSIDADAPI.getElementsOfTable($scope.CURIOSIDADArray);
    sabiasAPI.getElementsOfTable($scope.sabiasArray);
    var urlSabias = urlREST + "?method=get&table=sabias";
    var urlCosas = urlREST + "?method=get&table=cosas";
    $scope.noSabias = [];
    $scope.algunasCuriosidades = [];
    $http.get(urlSabias).success(function (yonosabia) {
        angular.forEach(yonosabia, function(yns) {
            yns.hijos=[];
            $scope.noSabias.push(yns);
        });
        $http.get(urlCosas).success(function (cosas) {
            angular.forEach($scope.noSabias, function(yns) {
                angular.forEach(cosas, function(estas) {
                    if(estas.idSabias == yns.id){
                        yns.hijos.push(estas);
                    }
                }); 
                if($scope.idSabias == yns.id){
                   $scope.curiosidadFilter = {
                      "id" : yns.hijos[0].id,
                      "idSabias" : $scope.idSabias
                    } 
                }
            });
            angular.forEach(cosas, function(estas) {
                $scope.algunasCuriosidades.push(estas);
            }); 
            // console.log($scope.noSabias);
        });
    });
});
/*readApp.controller('cosasCtrl', function($scope, $http, REST, $sce) {
    
    $scope.idSabias = get.id;
    $scope.idCuriosidad = curiosidad.id.toString();
    $scope.CURIOSIDADArray = [];
    $scope.sabiasArray = [];
    var tablaCtrl = "cosas";
    var CURIOSIDADAPI = new REST(urlREST, tablaCtrl);
    var sabiasAPI = new REST(urlREST, "sabias");

    $scope.formatoFecha = function(cadena){
      return dia(cadena) + " de " + mes(cadena) + " de " + ano(cadena);
    }


    CURIOSIDADAPI.getElementsOfTable($scope.CURIOSIDADArray);
    sabiasAPI.getElementsOfTable($scope.sabiasArray);
    console.log($scope.sabiasArray);
    console.log($scope.CURIOSIDADArray);
        
    $scope.nombreSabias = function(id){
      var index = indexJsonArray($scope.sabiasArray, id);

      return $scope.sabiasArray[index].nombre;
    }

    

    $scope.buscador = {
      "idSabias" : $scope.idSabias
    }
    $scope.curiosidadFilter = {
      "id" : $scope.idCuriosidad,
      "idSabias" : $scope.idSabias
    }

    $scope.asdf= function(c){
      $scope.curiosidadFilter = c;
    }

    $scope.strict=true;
});*/

readApp.controller('newsCtrl', function($scope, $http, REST, $sce, $filter){
    
  var urlNews = urlREST + "?method=get&table=noticias";
  $scope.newsArray = [];
  $http.get(urlNews).success(function (data) {
      angular.forEach(data, function(value) {
          if(value.idCategoria == categoria.id){
              $scope.newsArray.push(value);
          }
      });
  });

  $scope.preview = function(text){  
    var content= $filter('limitTo')(text, 300, 0);
    return $sce.trustAsHtml(content);
  }
  $scope.newGet =  categoria.id.toString();
  var urlCategorias = urlREST + "?method=get&table=categorias";
  $scope.categoriasArray = [];
  var urlSabias = urlREST + "?method=get&table=sabias";
  $scope.noSabias = [];
  $http.get(urlCategorias).success(function (data) {
    angular.forEach(data, function(value) {
        $scope.categoriasArray.push(value);
    });
      var index = indexJsonArray($scope.categoriasArray, $scope.newGet);
    $scope.nombreCategoria = $scope.categoriasArray[index];
      if($scope.nombreCategoria.nombre == 'Las 15 cosas de'){
          $http.get(urlSabias).success(function (yonosabia) {
              angular.forEach(yonosabia, function(esto) {
                  $scope.noSabias.push(esto);
              });
              // console.log($scope.NoSabias);
          });
      }
  });

  
  
  $scope.asdf = function(id){
    $scope.newGet.idCategoria = id;
  }


});

readApp.controller('singleCtrl', function($scope, $http, REST, $sce, $filter){

  var newsAPI = new REST(urlREST, "noticias");
  //newsAPI.getElementsOfTable($scope.newsArray);
  $scope.newsArray = [];
  $scope.newsCategoriesArray = [];
  $scope.idsBlobs = [];
  $scope.cat ={
      "id" : categoria.id.toString()
  }
  $scope.newGet = {
    "id" : single.id.toString()
  }
  var urlNoticias = urlREST + "?method=get&table=noticias";
  $http.get(urlNoticias).success(function (response) {
      $scope.newsArray = response;
      angular.forEach($scope.newsArray, function(value) {
          if(value.idCategoria == $scope.cat.id && value.id != $scope.newGet.id){
             $scope.newsCategoriesArray.push(value);
          }
      });
  });

  newsAPI.getBlobs($scope.idsBlobs, single.id.toString() );

});

readApp.factory('REST', 

  function($http) {
  
    var REST = function(url, tabla, limit) {
      if(limit === undefined || limit < 0 )
        limit = 0;

      this.url = url;
      this.tabla = tabla;
      this.limit = limit;
      this.clickCounter = 0;
    };

    REST.prototype.getElementsOfTable = function(arreglo){

      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;

      url = url + "?method=get&table=" + this.tabla ;

      if(this.limit != 0)
        url = url + "&limit=" + this.limit + "&offset=" + (this.clickCounter * this.limit);

      $http.get(url).success(
        function (data) {
          for (var i = 0; i < data.length; i++) {
              arreglo.push(data[i]);
          }
        }
      );
      
      this.clickCounter = this.clickCounter + 1;

      return arreglo;

    };

    REST.prototype.getData = function(arreglo){
    
      var url = this.url;

      $http.get(url).success(
        function (data) {
          for (var i = 0; i < data.length; i++) {
              arreglo.push(data[i]);
          }
        }
      );

      return arreglo;

    };


    REST.prototype.getBlobs = function(arreglo, idTabla){

      var url = this.url; 

      url = url + "?method=getBlobs&tablaBD=" + this.tabla + "&idTabla=" + idTabla ;

      $http.get(url).success(
        function (data) {
          for (var i = 0; i < data.length; i++) {
              arreglo.push(data[i]);
          }
        }
      );

      return arreglo;

    };

    REST.prototype.postData = function(arreglo, idArray, scopeActualizar){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;
      url = url + "?method=post&table=" + this.tabla;


      var request = $http({
          method: "post",
          url: url,
          data: arreglo,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
          //alert(JSON.stringify(response.data));
          idArray.id = response.data;
          arreglo.id = JSON.parse(idArray.id);
          scopeActualizar.push(arreglo);
      });

    };

    REST.prototype.updateData = function(arreglo){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;
      url = url + "?method=update&table=" + this.tabla;


      var request = $http({
          method: "post",
          url: url,
          data: arreglo,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
        //alert(JSON.stringify(response.data));
      });

    };

    REST.prototype.deleteAll = function (arreglo, deleteBlob){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }

      if(deleteBlob === undefined)
        deleteBlob = 0;

      var url = this.url;

      url = url + "?method=delete&table=" + this.tabla + "&blob=" + deleteBlob;

      var json = JSON.stringify(arreglo);

      var request = $http({
          method: "post",
          url: url,
          data: JSON.parse(json),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
          //alert(JSON.stringify(response.data));
      });
    };

    REST.prototype.postObjects = function (arreglo){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;

      url = url + "?method=postObjects&table=" + this.tabla;

      var json = JSON.stringify(arreglo);

      var request = $http({
          method: "post",
          url: url,
          data: JSON.parse(json),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
        alert(JSON.stringify(response.data));
      });
    };

    return REST;
  }

);

angular.bootstrap(document.getElementById("ReadApp"),['myApp3']);