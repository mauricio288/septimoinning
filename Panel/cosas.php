<?php
	$id = $_GET["id"];
	//***********************************************************
	//	I N C L U D E S
	//***********************************************************
	require("../php/requerirSesion.php");
	require("../php/selects.php");
  require("../php/jerarquias.php");
	//$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
  $current_url = "../Panel/cosas.php?id=".$id;
	
	
	//***********************************************************
	//	U S U A R I O
	//***********************************************************
	$nombreu = "";
	$correou = "";
	$perfilu = "";
	$sss= $_SESSION['userBaseball'];
  
	
	$result = resultados_consultar_tabla("usuarios", "WHERE id = $sss");
	while( $fila = mysqli_fetch_array( $result,MYSQLI_ASSOC ) )	{
		$nombreu = $fila["nombre"];
		$correou = $fila["email"];
		$perfilu = $fila["idPerfil"];
	}

  $page = access($perfilu, $current_url);

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Septimoinning  System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="../Tienda/template/images/favicon.png">-->
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
  <!-- the fixed layout is not compatible with sidebar-mini -->
  <body class="hold-transition skin-black fixed sidebar-mini" id="ReadApp" ng-controller="CURIOSIDADCtrl">
  	
  	<!-- Modal -->
  	<div class="modal fade" id="modalRegistrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	  <div class="modal-dialog" role="document">
  	    <div class="modal-content">
  	      <div class="modal-header">
  	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-picture-o"></i> Registrar nueva información</h4>
  	      </div>
  	      <div class="modal-body">
  	        <form name="myform" novalidate>
      			  <div class="form-group">
      			    <label>Nombre *</label>
      			    <input type="text" class="form-control" ng-model="formCURIOSIDAD.nombre" required>
                <input type="hidden" class="form-control" ng-model="formCURIOSIDAD.idSabias" >
      			  </div>
              <div class="form-group">
                <label>Texto *</label>
                <textarea type="text" class="form-control" ng-model="formCURIOSIDAD.texto" required></textarea>
              </div>
              <center>
      			  	<button type="submit" class="btn btn-success" ng-click="registrarCURIOSIDAD()" ng-show="myform.$valid">Siguiente</button>
      			  </center>
            </form>
  	      </div>
  	      <div class="modal-footer">
  	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  	      </div>
  	    </div>
  	  </div>
  	</div>

    <div class="modal fade" id="modalImagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel" ng-show="modalImagenAction == 'nuevo' ">Agregar imagen - {{modalImagenTabla}}</h4>
            <h4 class="modal-title" id="myModalLabel" ng-show="modalImagenAction == 'actualizar' ">Actualizar imagen - {{modalImagenTabla}}</h4>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <div class="well">

                  <form method="post" enctype="multipart/form-data" action="{{safeModalImagenURL()}}">
                    <div class="form-group">
                      <label for="exampleInputName2">Selecciona una nueva foto (Hasta 1 Mb)</label>
                      <input type="file" name="archivito" required>
                    </div>
                    <input type="hidden" name="url" value="<?php echo $current_url ?>">
                    <center>
                      <button type="submit" class="btn btn-success">Agregar</button>
                    </center>
                  </form>

                </div>
              </div>
            </div>
            

            <br>
            <center ng-show="modalImagenAction == 'actualizar' " >
              <img src="../php/visualizar_archivo.php?id={{lastID.id}}&tabla={{modalImagenTabla}}" class = "img-responsive img-rounded" >
            </center>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

	<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content" ng-repeat="CURIOSIDAD in CURIOSIDADEditar">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-picture-o"></i> Editar información #{{CURIOSIDAD.id}}</h4>
	      </div>
	      <div class="modal-body">
	        <form>
			  <div class="form-group">
			    <label>Nombre *</label>
			    <input type="text" class="form-control" ng-model="CURIOSIDAD.nombre">
			  </div>
              <div class="form-group">
                <label>Texto *</label>
                <textarea type="text" class="form-control" ng-model="CURIOSIDAD.texto" ></textarea>
              </div>
			  <center>
			  	<button type="submit" class="btn btn-primary" ng-click="editarCURIOSIDAD(CURIOSIDAD)">Guardar cambios</button>
          <br><br>
          <a ng-click="actualizarImagenCURIOSIDAD(CURIOSIDAD)">
            <b><i class="fa fa-picture-o" aria-hidden="true"></i> Ir a actualizar imagen</b>
          </a> 
			  </center>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>

    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">S<b>16</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Septimoinning</b> Admin</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li>

              	<a href="http://septimoinning.com/" target = "_BLANK" class="dropdown-toggle" >

                	<i class="fa fa-chrome"></i> <span class="hidden-xs">Ir al <b>sitio web</b></span>
                </a>
              </li>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="dist/img/logo.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $nombreu; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../php/visualizar_archivo.php?id=<?php echo $sss ?>&tabla=usuarios" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $nombreu; ?>
                      <small></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--<li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="perfil.php" class="btn btn-default"><i class="fa fa-user"></i> Editar perfil</a>
                    </div>
                    <div class="pull-right">
                      <a href="../php/cerrarSesion.php" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar sesión</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/logo.png" class="img-responsive" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nombreu; ?></p>
              
            </div>
          </div>
          <!-- search form -->
          <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Buscar productos...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
			
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Navegación Principal</li>
			<li>
              <a href="index.php">
                <i class="fa fa-home"></i> <span>Inicio</span>
              </a>
            </li>
      <li>
              <a href="noticias.php">
                <i class="fa fa-newspaper-o"></i> <span>Noticias</span>
              </a>
      </li>
			<li>
              <a href="banners.php">
                <i class="fa fa-picture-o"></i> <span>Banners</span>
              </a>
            </li>
        <li>
          <a href="sabias.php">
          <i class="fa fa-question-circle"></i> <span>15 Cosas que no sabías de...</span>
          </a>
        </li>
			<?php if($perfilu == 1){ ?>
				<li>
				  <a href="usuarios.php">
					<i class="fa fa-users"></i> <span>Acceso de usuarios</span>
				  </a>
				</li>
				
			<?php } ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <div class="btn-group" role="group" aria-label="...">
            <a href="sabias.php" type="button" class="btn btn-default" ><i class="fa fa-arrow-left"></i> Regresar</a>
          </div>
          <center>
            <h2>
              <i class="fa fa-question-circle"></i> {{nombreSabias(idSabias)}}<br>
              <small>Registra las 15 cosas de <strong>{{nombreSabias(idSabias)}}</strong>.</small>
            </h2>
            <div class="btn-group" role="group" aria-label="...">
              <button ng-show="elinfiltrao.length < 15 " type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalRegistrar"><i class="fa fa-question-circle"></i> Nueva información</button>
            </div>
        </center>
          
        </section>
        <br><br>
        <!-- Main content -->
        <section class="content">
			<div class = "row">
				<div class = "col-md-12">
					
          <div class="row" style="display:flex; flex-wrap: wrap;">
            <div class="col-md-4" ng-repeat = "CURIOSIDAD in elinfiltrao = (CURIOSIDADArray | filter: buscador) | orderBy:-id">
              <div class="thumbnail">
                <img src="../php/visualizar_archivo.php?id={{CURIOSIDAD.id}}&tabla=cosas">
                <div class="caption">
                  <h3>{{CURIOSIDAD.id}}-{{CURIOSIDAD.nombre}}</h3>
                  <p>{{CURIOSIDAD.texto}}</p>
                  <p>
                    <button type='button' class='btn btn-primary' ng-click="actualizarImagenCURIOSIDAD(CURIOSIDAD)">
                      <i class="fa fa-picture-o" aria-hidden="true"></i> <strong>Actualizar</strong> imagen
                    </button> <br><br>
                    <button type='button' class='btn btn-default' data-toggle='modal' data-target='#modalEditar' ng-click="filtrarCURIOSIDADEditar(CURIOSIDAD.id)">
                      <i class='fa fa-edit'></i> Editar información
                    </button> 
                    <button type='button' class='btn btn-danger'  ng-click="borrarCURIOSIDAD(CURIOSIDAD)">
                      <i class='fa fa-trash-o'></i> Eliminar
                    </button>
                  </p>
                </div>
              </div>
            </div>
          </div>
					
				</div>
				
				
				
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Code by <a href="https://www.facebook.com/CarlosU7" target = "_blank">Carlos Uscanga</a>. </strong>Design by &copy; 2014-2015 <a href="http://almsaeedstudio.com" target = "_blank">Almsaeed Studio</a>. All rights reserved.
      </footer>


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
	<!-- CarlosU7 Javascripts -->
	<script src="js-carlos/functions.js"></script>
    <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>
	
	<!-- Sugested search -->
	<script type="text/javascript" src="js/angular.min.js"></script>


  <script src='rich_text/textAngular-rangy.min.js'></script>
  <script src='rich_text/textAngular-sanitize.min.js'></script>
  <script src='rich_text/textAngular.min.js'></script>

  <script type="text/javascript">
    var get = { "id" : <?php echo $id ?> }
  </script>

	<script src="js/ui-bootstrap-tpls-0.9.0.js"></script>
	<script type="text/javascript" src="app/app.js"></script>
	
  </body>
</html>
