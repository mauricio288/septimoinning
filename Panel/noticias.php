<?php
	
	//***********************************************************
	//	I N C L U D E S
	//***********************************************************
	require("../php/requerirSesion.php");
	require("../php/selects.php");
  require("../php/jerarquias.php");

	$url = $_SERVER['REQUEST_URI'];


  $current_url = "../Panel/noticias.php";
	

	
	//***********************************************************
	//	U S U A R I O
	//***********************************************************
	$nombreu = "";
	$correou = "";
	$perfilu = "";
	$sss= $_SESSION['userBaseball'];
	
	$result = resultados_consultar_tabla("usuarios", "WHERE id = $sss");
	while( $fila = mysqli_fetch_array( $result,MYSQLI_ASSOC ) )	{
		$nombreu = $fila["nombre"];
		$correou = $fila["email"];
		$perfilu = $fila["idPerfil"];
	}

  access($perfilu, $current_url);

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Noticias | Septimoinning  System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="../Tienda/template/images/favicon.png">-->
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!--<link rel="stylesheet" href="http://textangular.com/dist/textAngular.css">-->
    <link rel="stylesheet" href="rich_text/textAngular.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
  <!-- the fixed layout is not compatible with sidebar-mini -->
  <body class="hold-transition skin-black fixed sidebar-mini" id="ReadApp" ng-controller="noticiasCtrl">
  	
  	<!-- Modal -->
  	<div class="modal fade" id="modalRegistrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	  <div class="modal-dialog modal-lg" role="document">
  	    <div class="modal-content">
  	      <div class="modal-header">
  	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  	        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-newspaper-o"></i> Registrar nueva noticia</h4>
  	      </div>
  	      <div class="modal-body">

  	        <form name="myform" novalidate> 
      			  <div class="form-group">
      			    <label>Título de la noticia *</label>
      			    <input type="text" class="form-control" ng-model="formNoticia.nombre" required>
      			  </div>

              <div class="form-group">
                <label>Autor *</label>
                <input type="text" class="form-control" ng-model="formNoticia.autor" required>
              </div>

              <div class="form-group">
                <label>Categoría *</label>
                <select class="form-control" ng-model="formNoticia.idCategoria" required>
                    <option ng-repeat="c in categoriasArray" ng-if="c.nombre!='Las 15 cosas de'" value="{{c.id}}">{{c.nombre}}</option>
                </select>
              </div> 

              <div class="form-group">
                <label>Contenido *</label>
                <!--<textarea class="form-control" rows="5" ng-model="formNoticia.contenido" required></textarea>-->
                <div text-angular ng-model="formNoticia.contenido"></div>
                <input type="hidden" class="form-control" ng-model="formNoticia.fecha">
                <input type="hidden" class="form-control" ng-model="formNoticia.hora">
              </div>

              

              <center>
      			    <button type="submit" class="btn btn-success" ng-click="registrarNoticia()" ng-show="myform.$valid">Registrar</button>
      			   </center>
            </form>

  	      </div>
  	      <div class="modal-footer">
  	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  	      </div>
  	    </div>
  	  </div>
  	</div>

    <div class="modal fade" id="modalImagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel" ng-show="modalImagenAction == 'nuevo' ">Agregar imagen - {{modalImagenTabla}}</h4>
            <h4 class="modal-title" id="myModalLabel" ng-show="modalImagenAction == 'actualizar' ">Actualizar imagen - {{modalImagenTabla}}</h4>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-md-8  col-md-offset-2">
                <div class="well">

                  <form method="post" enctype="multipart/form-data" action="{{safeModalImagenURL()}}">
                    <center>
                      <div class="form-group">
                        <label for="exampleInputName2">Selecciona una nueva foto (Hasta 1 Mb)</label>
                        <div class="form-group">
                          <div class="btn btn-default btn-file">
                            <i class="fa fa-paperclip"></i> Adjuntar archivo
                            <input type="file" name="archivito" required>
                          </div>
                          <p class="help-block">Max. 32MB</p>
                        </div>

                      </div>
                      <input type="hidden" name="url" value="<?php echo $current_url ?>">

                      <button type="submit" class="btn btn-success">Agregar</button>
                    </center>
                  </form>

                </div>  
              </div>
            </div>
            
            <br>
              <!-- GALERIA -->

              <div class ="row" ng-show="modalImagenAction == 'actualizar' " style="display:flex; flex-wrap: wrap;">
                <div class = "col-md-4" ng-repeat = "i in idsBlobs">
                  <img src="../php/visualizar_archivo.php?id={{i.id}}" class = "img-responsive img-rounded" >
                  <br>
                  <div class="form-group">
                    <center>
                      <button class="btn btn-primary btn-block" ng-click="setPrincipal(mynoticia, i.id)" ng-if="mynoticia.idArchivo != i.id">Establecer como principal</button>
                      <button class="btn btn-danger btn-block" ng-click="borrarArchivo(i)">Eliminar</button>
                    </center>
                  </div>
                </div>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

	<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content" ng-repeat="noticia in noticiaEditar">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-newspaper-o"></i> Editar noticia #{{noticia.id}}</h4>
	      </div>
	      <div class="modal-body">
	        <form>
			  <div class="form-group">
			    <label>Título de la noticia*</label>
			    <input type="text" class="form-control" ng-model="noticia.nombre">
			  </div>

          <div class="form-group">
            <label>Autor *</label>
            <input type="text" class="form-control" ng-model="noticia.autor">
          </div>

          <div class="form-group">
            <label>Categoría *</label>
            <!--<select class="form-control" ng-model="noticia.idCategoria" ng-options="c.id as ( c.nombre ) for c in categoriasArray"></select>-->
            <select class="form-control" ng-model="noticia.idCategoria">
                <option ng-repeat="c in categoriasArray" ng-if="c.nombre!='Las 15 cosas de'" value="{{c.id}}">{{c.nombre}}</option>
            </select>
          </div> 

          <div class="form-group">
            <label>Contenido *</label>
            <div text-angular ng-model="noticia.contenido"></div>
          </div>

			  <center>
			  	<button type="submit" class="btn btn-primary" ng-click="editarNoticia(noticia)">Guardar cambios</button>
          <br><br>
          <a ng-click="actualizarImagenNoticia(noticia)">
            <b><i class="fa fa-picture-o" aria-hidden="true"></i> Ir a actualizar imagen</b>
          </a> 
			  </center>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>

    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">S<b>16</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Septimoinning</b> Admin</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->

              	<a href="http://septimoinning.com/" target = "_BLANK" class="dropdown-toggle" >

                	<i class="fa fa-chrome"></i> <span class="hidden-xs">Ir al <b>sitio web</b></span>
                </a>
              </li>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="dist/img/logo.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $nombreu; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../php/visualizar_archivo.php?id=<?php echo $sss ?>&tabla=usuarios" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $nombreu; ?>
                      <small></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--<li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="perfil.php" class="btn btn-default"><i class="fa fa-user"></i> Editar perfil</a>
                    </div>
                    <div class="pull-right">
                      <a href="../php/cerrarSesion.php" class="btn btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar sesión</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/logo.png" class="img-responsive" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $nombreu; ?></p>
              
            </div>
          </div>
          <!-- search form -->
          <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Buscar productos...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
      
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Navegación Principal</li>
      <li class = "active">
              <a href="index.php">
                <i class="fa fa-home"></i> <span>Inicio</span>
              </a>
            </li>
            <li>
              <a href="noticias.php">
                <i class="fa fa-newspaper-o"></i> <span>Noticias</span>
              </a>
            </li>
        <li>
          <a href="sabias.php">
          <i class="fa fa-question-circle"></i> <span>15 Cosas que no sabías de...</span>
          </a>
        </li>
        
      <?php if($perfilu == 1){ ?>
        <li>
          <a href="banners.php">
          <i class="fa fa-picture-o"></i> <span>Banners</span>
          </a>
        </li>
        <li>
          <a href="publicidad.php">
            <i class="fa fa-bullhorn"></i> <span>Publicidad</span>
          </a>
        </li>
        <li>
          <a href="usuarios.php">
          <i class="fa fa-users"></i> <span>Acceso de usuarios</span>
          </a>
        </li>
        
      <?php } ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <center><h1>
            <i class="fa fa-newspaper-o"></i> Noticias<br>
            <small>Altas, bajas, consultas y actualizaciones.</small>
          </h1></center>
		  <ol class="breadcrumb">
            <div class="btn-group" role="group" aria-label="...">
				      <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalRegistrar"><i class="fa fa-newspaper-o"></i> Nueva noticia</button>
			      </div>
      </ol>
          
        </section>

        <!-- Main content -->
        <section class="content">
			<div class = "row">
				<div class = "col-md-12">

					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
						  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><h4><i class="fa fa-newspaper-o"></i> Noticias</h4></a></li>
						  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
						</ul>
						<div class="tab-content">
						  <div class="tab-pane active" id="tab_1">
							<div class="box">
								<div class="box-header">
									<div class="row">
                    <div class = "col-md-4 col-md-offset-4">
                      <h4 ng-show="buscador"><strong>Resultados: </strong><em>"{{buscador}}"</em></h4>
                      <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-search"></i></div>
                        <input type="text" class="form-control" placeholder="Buscar..." ng-model="buscador">
                      </div>
                    </div>
                  </div>
								</div>
								<div class="box-body table-responsive no-padding">
										<table class="table table-striped">
											<tbody>
												<tr>
													<th>
  														<button type='button' class='btn btn-default' style = "color: #DD4B39;" ng-click="deleteNoticias()">
  															<i class='fa fa-trash-o'></i>
  														</button>
													</th>
													<th><center>#</center></th>
													<th><center>Título</center></th>
                          <th><center>Información</center></th>
                          <th><center>Galería de imágenes</center></th>
													<th width="100px;"><center>Opciones</center></th>
												</tr>
														
												<tr ng-repeat = "noticia in noticiasArray | filter: buscador">
													<td><input type='checkbox' ng-click="agregarNoticiasEliminar(noticia)"></td>
													<td>{{noticia.id}}</td>
													<td><h4><b>{{noticia.nombre}}</b></h4></td>
													<td>
                            <h4><strong>Categoría:</strong> {{nombreCategoria(noticia.idCategoria)}}</h4>
                            <strong>Autor:</strong> {{noticia.autor}}<br>
                            <strong>Fecha de publicación: </strong>{{formatoFecha(noticia.fecha)}} a las {{noticia.hora}}<br><br>
                            <strong>Contenido</strong> - <a data-toggle="collapse" href="#collapseExample{{noticia.id}}" aria-expanded="false" aria-controls="collapseExample">Ver info...</a><br>
                            <div class="collapse" id="collapseExample{{noticia.id}}">
                              <div ng-bind-html="noticia.contenido"></div>
                            </div>
                          </td>
                          <td>
                            <center>
                              <button type='button' class='btn btn-info' ng-click="actualizarImagenNoticia(noticia)">
                                <i class="fa fa-picture-o" aria-hidden="true"></i> Ver galería
                              </button> 
                            </center>
                          </td>
													<td>
														<center>
															<div class='tools'>
																<button type='button' class='btn btn-default' data-toggle='modal' data-target='#modalEditar' ng-click="filtrarNoticiaEditar(noticia.id)">
																	<i class='fa fa-edit'></i>
																</button>	
																<button type='button' class='btn btn-danger'  ng-click="borrarNoticia(noticia)">
																	<i class='fa fa-trash-o'></i>
																</button>
															</div>
														</center>
													</td>
												</tr>
														
											</tbody>
										</table>
								</div><!-- /.box-body -->
							</div>
						  </div><!-- /.tab-pane -->
						</div><!-- /.tab-content -->
					</div>
				
					
				</div>
				
				
				
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Code by <a href="https://www.facebook.com/CarlosU7" target = "_blank">Carlos Uscanga</a>. </strong>Design by &copy; 2014-2015 <a href="http://almsaeedstudio.com" target = "_blank">Almsaeed Studio</a>. All rights reserved.
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Page Script -->
    <script>
      $(function () {
        //Add text editor
        $("#compose-textarea").wysihtml5();
      });
    </script>

	<!-- CarlosU7 Javascripts -->
	<script src="js-carlos/functions.js"></script>
    <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>


  <script type="text/javascript" src="js/angular.min.js"></script>

  <!--<script src='http://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.0/textAngular-rangy.min.js'></script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.0/textAngular-sanitize.min.js'></script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/textAngular/1.5.0/textAngular.min.js'></script>-->
  
  <script src='rich_text/textAngular-rangy.min.js'></script>
  <script src='rich_text/textAngular-sanitize.min.js'></script>
  <script src='rich_text/textAngular.min.js'></script>

	
	<!-- Sugested search -->
	<script src="js/ui-bootstrap-tpls-0.9.0.js"></script>
	<script type="text/javascript" src="app/app.js"></script>




	
  </body>
</html>
