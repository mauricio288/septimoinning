function indexJsonArray(array, id){
  var index = -1;
  for(var i=0; i < array.length; i++){
    if(array[i].id == id){
      index = i;
      break;
    }
  }
  return index;
}

function getFechaActual(){
  var d = new Date();
  var dia = d.getDate();
  var mes = d.getMonth() + 1;
  var ano = d.getFullYear();
  if(mes < 10)
    mes = "0" + mes;
  if(dia < 10)
    dia = "0" + dia;
  var fecha = ano + "-" + mes + "-" + dia;
  return fecha;
}

function getHoraActual(){
  var d = new Date();
  var hora = d.getHours();
  var minuto = d.getMinutes();
  if(hora < 10)
    hora = "0" + hora;
  if(minuto < 10)
    minuto = "0" + minuto;
  var horario = hora + ":" + minuto;

  return horario;
}

function ano(cadena){

  return cadena[0]+cadena[1]+cadena[2]+cadena[3];
}

function mes(cadena){
  var mes = cadena[5] + cadena[6];
  
  switch(mes){
    case "01":
      return "Enero";
    case "02":
      return "Febrero";
    case "03":
      return "Marzo";
    case "04":
      return "Abril";
    case "05":
      return "Mayo";
    case "06":
      return "Junio";
    case "07":
      return "Julio";
    case "08":
      return "Agosto";
    case "09":
      return "Septiembre";  
    case "10":
      return "Octubre";
    case "11":
      return "Noviembre";
    case "12":
      return "Diciembre";
  }

}

function dia(cadena){

  return cadena[8] + cadena[9];
}

var readApp = angular.module('myApp3', ['textAngular']);

readApp.controller('inicioCtrl', function($scope, $http, REST, $sce) {

    $scope.suh = "hola mundo";

    $scope.editarContacto = function(contacto){
      alert("Información atualizada exitosamente.")
      contactoAPI.updateData(contacto);
    }

    var contactoAPI = new REST("../php/mainSQL.php", "contacto");

    $scope.contactoArray = [];

    contactoAPI.getElementsOfTable($scope.contactoArray);
});

readApp.controller('usuariosCtrl', function($scope, $http, REST, $sce) {

    $scope.registrarUsuario = function(){
        $("#modalRegistrar").modal('hide');
        usuariosAPI.postData($scope.formUsuario, $scope.lastID, $scope.usuariosArray );
        $scope.formUsuario = {};
    }

    $scope.agregarUsuariosEliminar = function (usuario) {
      var index = indexJsonArray($scope.usuariosBorrar, usuario.id);

      if(index == -1)
        $scope.usuariosBorrar.push(usuario);
      else
        $scope.usuariosBorrar.splice(index, 1);
    }

    $scope.deleteUsuarios = function(){
      var flag = confirm("¿Estas seguro que quieres borrar los usuarios seleccionados?");
      
      if(flag){
      
        usuariosAPI.deleteAll($scope.usuariosBorrar, "1");

        for(var i=0; i< $scope.usuariosBorrar.length; i++){
          var index = indexJsonArray($scope.usuariosArray, $scope.usuariosBorrar[i].id);
          $scope.usuariosArray.splice(index, 1);
        }


        $scope.usuariosBorrar = [];
      }
    }

    $scope.borrarUsuario = function(usuario){
      var flag = confirm("¿Estás seguro que quieres borrar a " + usuario.id + "?");
      
      if(flag){
        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON
        var arreglo = [];
        arreglo.push(usuario);
        usuariosAPI.deleteAll(arreglo, "1");

        var index = indexJsonArray($scope.usuariosArray, usuario.id);
        $scope.usuariosArray.splice(index, 1);
      }
    }

    $scope.filtrarUsuarioEditar = function(idUsuario){
      $scope.usuarioEditar = [];
      $scope.usuarioEditar = $scope.usuariosArray.filter(function(usuario) {
        return usuario.id == idUsuario;
      })
    }

    $scope.editarUsuario = function(usuario){
      $("#modalEditar").modal('hide');
      usuariosAPI.updateData(usuario);
    }

    $scope.nombrePerfil = function(id){
      var index = indexJsonArray($scope.perfilesArray, id);

      return $scope.perfilesArray[index].nombre;
    }

    var usuariosAPI = new REST("../php/mainSQL.php", "usuarios");
    var perfilesAPI = new REST("../php/mainSQL.php", "perfiles");

    $scope.idSesion = idSesion;
    $scope.usuariosArray = [];
    $scope.perfilesArray = [];
    $scope.usuariosBorrar = [];
    $scope.lastID = {"id" : 0};
    $scope.formUsuario = {}

    usuariosAPI.getElementsOfTable($scope.usuariosArray);
    perfilesAPI.getElementsOfTable($scope.perfilesArray);

});

readApp.controller('noticiasCtrl', function($scope, $http, REST, $sce) {

    $scope.mostrarModalImagen = function(tabla, action){
      $scope.modalImagenTabla = tabla;
      $scope.modalImagenAction = action;
      $("#modalImagen").modal('show');
    }

    $scope.safeModalImagenURL = function(){
      var id;

      id = JSON.parse($scope.lastID.id);    

      var link = "../php/sqlMain.php?crud=registrarBlob&table="+ $scope.modalImagenTabla + "&id=" + id;

      return $sce.trustAsResourceUrl(link);
    }

    $scope.registrarNoticia = function(){
      
        $("#modalRegistrar").modal('hide');

        $scope.formNoticia.hora = getHoraActual();
        $scope.formNoticia.fecha = getFechaActual();

        noticiasAPI.postData($scope.formNoticia, $scope.lastID, $scope.noticiasArray );
        $scope.formNoticia = {};
        $scope.mostrarModalImagen(tablaCtrl, "nuevo");
    }

    $scope.actualizarImagenNoticia = function(noticia){
      $("#modalEditar").modal('hide');
      $scope.lastID.id = noticia.id;
      $scope.mynoticia = noticia;
      $scope.mostrarModalImagen(tablaCtrl, "actualizar");

      $scope.galeria(noticia.id);
    }
    
    $scope.setPrincipal = function(noticia, id){
        noticia.idArchivo = id;
        noticiasAPI.updateData(noticia);
    }

    $scope.borrarArchivo = function(archivo){
      var flag = confirm("¿Estás seguro que deseas borrar esta imagen? ("+ archivo.id +")");

      if(flag){
        var archivosAPI = new REST("../php/mainSQL.php", "archivos");

        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON
        var arreglo = [];
        arreglo.push(archivo);
        //Borrar en el backend
        archivosAPI.deleteAll(arreglo);

        var index = indexJsonArray($scope.idsBlobs, archivo.id);
        $scope.idsBlobs.splice(index, 1);

      }
    }

    $scope.galeria = function(id){
      $scope.idsBlobs = [];
      noticiasAPI.getBlobs($scope.idsBlobs, id);
    }

    $scope.agregarNoticiasEliminar = function (noticia) {
      var index = indexJsonArray($scope.noticiasBorrar, noticia.id);

      if(index == -1)
        $scope.noticiasBorrar.push(noticia);
      else
        $scope.noticiasBorrar.splice(index, 1);
    }

    $scope.deleteNoticias = function(){
      var flag = confirm("¿Estas seguro que quieres borrar los noticias seleccionados?");
      
      if(flag){
      
        noticiasAPI.deleteAll($scope.noticiasBorrar, "1");

        for(var i=0; i< $scope.noticiasBorrar.length; i++){
          var index = indexJsonArray($scope.noticiasArray, $scope.noticiasBorrar[i].id);
          $scope.noticiasArray.splice(index, 1);
        }


        $scope.noticiasBorrar = [];
      }
    }

    $scope.borrarNoticia = function(noticia){
      var flag = confirm("¿Estás seguro que quieres borrar la noticia #" + noticia.id + "?");
      
      if(flag){
        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON
        var arreglo = [];
        arreglo.push(noticia);
        noticiasAPI.deleteAll(arreglo, "1");

        var index = indexJsonArray($scope.noticiasArray, noticia.id);
        $scope.noticiasArray.splice(index, 1);
      }
    }

    $scope.filtrarNoticiaEditar = function(idNoticia){
      $scope.noticiaEditar = [];
      $scope.noticiaEditar = $scope.noticiasArray.filter(function(noticia) {
        return noticia.id == idNoticia;
      })
    }

    $scope.editarNoticia = function(noticia){
      $("#modalEditar").modal('hide');
      noticiasAPI.updateData(noticia);
    }

    $scope.formatoFecha = function(cadena){

      return dia(cadena) + " de " + mes(cadena) + " de " + ano(cadena);
    }

    $scope.nombreCategoria = function(id){
      var index = indexJsonArray($scope.categoriasArray, id);

      return $scope.categoriasArray[index].nombre;
    }

    var noticiasAPI = new REST("../php/mainSQL.php", "noticias");
    var categoriasAPI = new REST("../php/mainSQL.php", "categorias")
    var tablaCtrl = "noticias";

    $scope.noticiasArray = [];
    $scope.categoriasArray = [];
    $scope.noticiasBorrar = [];
    $scope.lastID = {"id" : 0};

    noticiasAPI.getElementsOfTable($scope.noticiasArray);
    categoriasAPI.getElementsOfTable($scope.categoriasArray);

});

readApp.controller('bannersCtrl', function($scope, $http, REST, $sce) {

    $scope.mostrarModalImagen = function(tabla, action){
      $scope.modalImagenTabla = tabla;
      $scope.modalImagenAction = action;
      $("#modalImagen").modal('show');
    }

    $scope.safeModalImagenURL = function(){
      var id;

      id = JSON.parse($scope.lastID.id);    

      var link = "../php/sqlMain.php?crud=blobNuevo&table="+ $scope.modalImagenTabla + "&id=" + id;

      return $sce.trustAsResourceUrl(link);
    }

    $scope.registrarBanner = function(){
      if($scope.formBanner.nombre == "")
        alert("Favor de ingresar el nombre.")
      else{
        $("#modalRegistrar").modal('hide');
        bannersAPI.postData($scope.formBanner, $scope.lastID, $scope.bannersArray );
        $scope.formBanner = {
          "nombre" : ""
        };
        $scope.mostrarModalImagen(tablaCtrl, "nuevo");
      }
    }

    $scope.actualizarImagenBanner = function(banner){
      $("#modalEditar").modal('hide');
      $scope.lastID.id = banner.id;
      $scope.mostrarModalImagen(tablaCtrl, "actualizar");
    }

    $scope.agregarBannersEliminar = function (banner) {
      var index = indexJsonArray($scope.bannersBorrar, banner.id);

      if(index == -1)
        $scope.bannersBorrar.push(banner);
      else
        $scope.bannersBorrar.splice(index, 1);
    }

    $scope.deleteBanners = function(){
      var flag = confirm("¿Estas seguro que quieres borrar los banners seleccionados?");
      
      if(flag){
      
        bannersAPI.deleteAll($scope.bannersBorrar, "1");

        for(var i=0; i< $scope.bannersBorrar.length; i++){
          var index = indexJsonArray($scope.bannersArray, $scope.bannersBorrar[i].id);
          $scope.bannersArray.splice(index, 1);
        }


        $scope.bannersBorrar = [];
      }
    }

    $scope.borrarBanner = function(banner){
      var flag = confirm("¿Estás seguro que quieres borrar el banner #" + banner.id + "?");
      
      if(flag){
        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON
        var arreglo = [];
        arreglo.push(banner);
        bannersAPI.deleteAll(arreglo, "1");

        var index = indexJsonArray($scope.bannersArray, banner.id);
        $scope.bannersArray.splice(index, 1);
      }
    }

    $scope.filtrarBannerEditar = function(idBanner){
      $scope.bannerEditar = [];
      $scope.bannerEditar = $scope.bannersArray.filter(function(banner) {
        return banner.id == idBanner;
      })
    }

    $scope.editarBanner = function(banner){
      $("#modalEditar").modal('hide');
      bannersAPI.updateData(banner);
    }


    var bannersAPI = new REST("../php/mainSQL.php", "banners");
    var tablaCtrl = "banners";

    $scope.bannersArray = [];
    $scope.bannersBorrar = [];
    $scope.lastID = {"id" : 0};
    $scope.formBanner = {
      "nombre" : ""
    }

    bannersAPI.getElementsOfTable($scope.bannersArray);

});

readApp.controller('publicidadCtrl', function($scope, $http, REST, $sce) {

    $scope.mostrarModalImagen = function(tabla, action){
      $scope.modalImagenTabla = tabla;
      $scope.modalImagenAction = action;
      $("#modalImagen").modal('show');
    }

    $scope.safeModalImagenURL = function(){
      var id;

      id = JSON.parse($scope.lastID.id);    

      var link = "../php/sqlMain.php?crud=blobNuevo&table="+ $scope.modalImagenTabla + "&id=" + id;

      return $sce.trustAsResourceUrl(link);
    }

    $scope.registrarPublicidad = function(){
      if($scope.formPublicidad.nombre == "")
        alert("Favor de ingresar el nombre.")
      else{
        if($scope.formPublicidad.url == ""){
            alert("Favor de ingresar una URL.")
        }else{
            $("#modalRegistrar").modal('hide');
            publicidadAPI.postData($scope.formPublicidad, $scope.lastID, $scope.publicidadArray );
            $scope.formPublicidad = {
              "nombre" : "",
              "url" : ""
            };
            $scope.mostrarModalImagen(tablaCtrl, "nuevo");
        }
      }
    }

    $scope.actualizarImagenPublicidad = function(pub){
      $("#modalEditar").modal('hide');
      $scope.lastID.id = pub.id;
      $scope.mostrarModalImagen(tablaCtrl, "actualizar");
    }

    $scope.agregarPublicidadEliminar = function (pub) {
      var index = indexJsonArray($scope.publicidadBorrar, pub.id);

      if(index == -1)
        $scope.publicidadBorrar.push(pub);
      else
        $scope.publicidadBorrar.splice(index, 1);
    }

    $scope.deletePublicidad = function(){
      var flag = confirm("¿Estas seguro que quieres borrar los anuncios seleccionados?");
      
      if(flag){
      
        publicidadAPI.deleteAll($scope.publicidadBorrar, "1");

        for(var i=0; i< $scope.publicidadBorrar.length; i++){
          var index = indexJsonArray($scope.publicidadArray, $scope.publicidadBorrar[i].id);
          $scope.publicidadArray.splice(index, 1);
        }


        $scope.publicidadBorrar = [];
      }
    }

    $scope.borrarPublicidad = function(pub){
      var flag = confirm("¿Estás seguro que quieres borrar la publicidad #" + pub.id + "?");
      
      if(flag){
        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON
        var arreglo = [];
        arreglo.push(pub);
        publicidadAPI.deleteAll(arreglo, "1");

        var index = indexJsonArray($scope.publicidadArray, pub.id);
        $scope.publicidadArray.splice(index, 1);
      }
    }

    $scope.filtrarPublicidadEditar = function(idPub){
      $scope.publicidadEditar = [];
      $scope.publicidadEditar = $scope.publicidadArray.filter(function(pub) {
        return pub.id == idPub;
      })
    }

    $scope.editarPublicidad = function(pub){
      $("#modalEditar").modal('hide');
      publicidadAPI.updateData(pub);
    }


    var publicidadAPI = new REST("../php/mainSQL.php", "publicidad");
    var tablaCtrl = "publicidad";

    $scope.publicidadArray = [];
    $scope.publicidadBorrar = [];
    $scope.lastID = {"id" : 0};
    $scope.formPublicidad = {
      "nombre" : "",
      "url" : ""
    }

    publicidadAPI.getElementsOfTable($scope.publicidadArray);

});

readApp.controller('cosasCtrl', function($scope, $http, REST, $sce) {

    $scope.mostrarModalImagen = function(tabla, action){
      $scope.modalImagenTabla = tabla;
      $scope.modalImagenAction = action;
      $("#modalImagen").modal('show');
    }

    $scope.safeModalImagenURL = function(){
      var id;

      id = JSON.parse($scope.lastID.id);    

      var link = "../php/sqlMain.php?crud=blobNuevo&table="+ $scope.modalImagenTabla + "&id=" + id;

      return $sce.trustAsResourceUrl(link);
    }

    $scope.registrarCosa = function(){
      
        $("#modalRegistrar").modal('hide');

        $scope.formCosa.hora = getHoraActual();
        $scope.formCosa.fecha = getFechaActual();

        cosasAPI.postData($scope.formCosa, $scope.lastID, $scope.cosasArray );
        $scope.mostrarModalImagen(tablaCtrl, "nuevo");
  
    }

    $scope.actualizarImagenCosa = function(cosa){
      $("#modalEditar").modal('hide');
      $scope.lastID.id = cosa.id;
      $scope.mostrarModalImagen(tablaCtrl, "actualizar");
    }

    $scope.agregarCosasEliminar = function (cosa) {
      var index = indexJsonArray($scope.cosasBorrar, cosa.id);

      if(index == -1)
        $scope.cosasBorrar.push(cosa);
      else
        $scope.cosasBorrar.splice(index, 1);
    }

    $scope.deleteCosas = function(){
      var flag = confirm("¿Estas seguro que quieres borrar los registros seleccionados?");
      
      if(flag){
      
        cosasAPI.deleteAll($scope.cosasBorrar, "1");

        for(var i=0; i< $scope.cosasBorrar.length; i++){
          var index = indexJsonArray($scope.cosasArray, $scope.cosasBorrar[i].id);
          $scope.cosasArray.splice(index, 1);
        }


        $scope.cosasBorrar = [];
      }
    }

    $scope.borrarCosa = function(cosa){
      var flag = confirm("¿Estás seguro que quieres borrar la información #" + cosa.id + "?");
      
      if(flag){
        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON
        var arreglo = [];
        arreglo.push(cosa);
        cosasAPI.deleteAll(arreglo, "1");

        var index = indexJsonArray($scope.cosasArray, cosa.id);
        $scope.cosasArray.splice(index, 1);
      }
    }

    $scope.filtrarCosaEditar = function(idCosa){
      $scope.cosaEditar = [];
      $scope.cosaEditar = $scope.cosasArray.filter(function(cosa) {
        return cosa.id == idCosa;
      })
    }

    $scope.editarCosa = function(cosa){
      $("#modalEditar").modal('hide');
      cosasAPI.updateData(cosa);
    }

    $scope.formatoFecha = function(cadena){

      return dia(cadena) + " de " + mes(cadena) + " de " + ano(cadena);
    }

    var tablaCtrl = "sabias";
    var cosasAPI = new REST("../php/mainSQL.php", tablaCtrl);
    

    $scope.cosasArray = [];
    $scope.cosasBorrar = [];
    $scope.lastID = {"id" : 0};
    $scope.formCosa = {
      "nombre" : ""
    }

    cosasAPI.getElementsOfTable($scope.cosasArray);

});

readApp.controller('CURIOSIDADCtrl', function($scope, $http, REST, $sce) {

    $scope.mostrarModalImagen = function(tabla, action){
      $scope.modalImagenTabla = tabla;
      $scope.modalImagenAction = action;
      $("#modalImagen").modal('show');
    }

    $scope.safeModalImagenURL = function(){
      var id;

      id = JSON.parse($scope.lastID.id);    

      var link = "../php/sqlMain.php?crud=blobNuevo&table="+ $scope.modalImagenTabla + "&id=" + id;

      return $sce.trustAsResourceUrl(link);
    }

    $scope.registrarCURIOSIDAD = function(){
      
        $("#modalRegistrar").modal('hide');
        $scope.formCURIOSIDAD.idSabias = $scope.idSabias;
        CURIOSIDADAPI.postData($scope.formCURIOSIDAD, $scope.lastID, $scope.CURIOSIDADArray );
        $scope.mostrarModalImagen(tablaCtrl, "nuevo");
  
    }

    $scope.actualizarImagenCURIOSIDAD = function(CURIOSIDAD){
      $("#modalEditar").modal('hide');
      $scope.lastID.id = CURIOSIDAD.id;
      $scope.mostrarModalImagen(tablaCtrl, "actualizar");
    }

    $scope.agregarCURIOSIDADEliminar = function (CURIOSIDAD) {
      var index = indexJsonArray($scope.CURIOSIDADBorrar, CURIOSIDAD.id);

      if(index == -1)
        $scope.CURIOSIDADBorrar.push(CURIOSIDAD);
      else
        $scope.CURIOSIDADBorrar.splice(index, 1);
    }

    $scope.deleteCURIOSIDAD = function(){
      var flag = confirm("¿Estas seguro que quieres borrar los registros seleccionados?");
      
      if(flag){
      
        CURIOSIDADAPI.deleteAll($scope.CURIOSIDADBorrar, "1");

        for(var i=0; i< $scope.CURIOSIDADBorrar.length; i++){
          var index = indexJsonArray($scope.CURIOSIDADArray, $scope.CURIOSIDADBorrar[i].id);
          $scope.CURIOSIDADArray.splice(index, 1);
        }


        $scope.CURIOSIDADBorrar = [];
      }
    }

    $scope.borrarCURIOSIDAD = function(CURIOSIDAD){
      var flag = confirm("¿Estás seguro que quieres borrar la información #" + CURIOSIDAD.id + "?");
      
      if(flag){
        //Arreglo JSON para acomodar el elemento y que el php pueda reconocerlo como objeto JSON
        var arreglo = [];
        arreglo.push(CURIOSIDAD);
        CURIOSIDADAPI.deleteAll(arreglo, "1");

        var index = indexJsonArray($scope.CURIOSIDADArray, CURIOSIDAD.id);
        $scope.CURIOSIDADArray.splice(index, 1);
      }
    }

    $scope.filtrarCURIOSIDADEditar = function(idCURIOSIDAD){
      $scope.CURIOSIDADEditar = [];
      $scope.CURIOSIDADEditar = $scope.CURIOSIDADArray.filter(function(CURIOSIDAD) {
        return CURIOSIDAD.id == idCURIOSIDAD;
      })
    }

    $scope.editarCURIOSIDAD = function(CURIOSIDAD){
      $("#modalEditar").modal('hide');
      CURIOSIDADAPI.updateData(CURIOSIDAD);
    }

    $scope.formatoFecha = function(cadena){

      return dia(cadena) + " de " + mes(cadena) + " de " + ano(cadena);
    }

    $scope.nombreSabias = function(id){
      var index = indexJsonArray($scope.sabiasArray, id);

      return $scope.sabiasArray[index].nombre;
    }

    var tablaCtrl = "cosas";
    var CURIOSIDADAPI = new REST("../php/mainSQL.php", tablaCtrl);
    var sabiasAPI = new REST("../php/mainSQL.php", "sabias");
    
    $scope.idSabias = get.id;

    $scope.CURIOSIDADArray = [];
    $scope.sabiasArray = [];
    $scope.CURIOSIDADBorrar = [];
    $scope.lastID = {"id" : 0};
    $scope.buscador = {
      "idSabias" : $scope.idSabias
    }

    CURIOSIDADAPI.getElementsOfTable($scope.CURIOSIDADArray);
    sabiasAPI.getElementsOfTable($scope.sabiasArray);

});

readApp.factory('REST', 

  function($http) {
  
    var REST = function(url, tabla, limit) {
      if(limit === undefined || limit < 0 )
        limit = 0;

      this.url = url;
      this.tabla = tabla;
      this.limit = limit;
      this.clickCounter = 0;
    };

    REST.prototype.getElementsOfTable = function(arreglo){

      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;

      url = url + "?method=get&table=" + this.tabla ;

      if(this.limit != 0)
        url = url + "&limit=" + this.limit + "&offset=" + (this.clickCounter * this.limit);

      $http.get(url).success(
        function (data) {
          for (var i = 0; i < data.length; i++) {
              arreglo.push(data[i]);
          }
        }
      );
      
      this.clickCounter = this.clickCounter + 1;

      return arreglo;

    };

    REST.prototype.getBlobs = function(arreglo, idTabla){

      var url = this.url;

      url = url + "?method=getBlobs&tablaBD=" + this.tabla + "&idTabla=" + idTabla ;

      $http.get(url).success(
        function (data) {
          for (var i = 0; i < data.length; i++) {
              arreglo.push(data[i]);
          }
        }
      );

      return arreglo;

    };

    REST.prototype.postData = function(arreglo, idArray, scopeActualizar){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;
      url = url + "?method=post&table=" + this.tabla;


      var request = $http({
          method: "post",
          url: url,
          data: arreglo,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
          //alert(JSON.stringify(response.data));
          idArray.id = response.data;
          arreglo.id = JSON.parse(idArray.id);
          idArray.id = JSON.parse(idArray.id);
          scopeActualizar.push(arreglo);
      });

    };

    REST.prototype.updateData = function(arreglo){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;
      url = url + "?method=update&table=" + this.tabla;


      var request = $http({
          method: "post",
          url: url,
          data: arreglo,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
        //alert(JSON.stringify(response.data));
      });

    };

    REST.prototype.deleteAll = function (arreglo, deleteBlob){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }

      if(deleteBlob === undefined)
        deleteBlob = 0;

      var url = this.url;

      url = url + "?method=delete&table=" + this.tabla + "&blob=" + deleteBlob;

      var json = JSON.stringify(arreglo);

      var request = $http({
          method: "post",
          url: url,
          data: JSON.parse(json),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
          //alert(JSON.stringify(response.data));
      });
    };

    REST.prototype.postObjects = function (arreglo){
      if(this.tabla === undefined){
        alert("no se ha definido una tabla");
        return;
      }
      var url = this.url;

      url = url + "?method=postObjects&table=" + this.tabla;

      var json = JSON.stringify(arreglo);

      var request = $http({
          method: "post",
          url: url,
          data: JSON.parse(json),
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

      request.then(function (response) {
        alert(JSON.stringify(response.data));
      });
    };

    return REST;
  }

);

angular.bootstrap(document.getElementById("ReadApp"),['myApp3']);