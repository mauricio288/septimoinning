		function navegarm(){
			var url = document.datosm.sitiosm.value;
			window.location.href = url;
		}
		
		function navegard(){
			var url = document.datosd.sitiosd.value;
			window.location.href = url;
		}
		
		function navegarl(){
			var url = document.datosl.sitiosl.value;
			window.location.href = url;
		}

		function editAJAX(str, tabla) {
			if (str=="") {
				document.getElementById("txtHint").innerHTML="";
				return;
			} 
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else { // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					//alert("formEdit"+tabla);
				  document.getElementById("formEdit" + tabla).innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","formEdit"+tabla+".php?id="+str,true);
			xmlhttp.send();
		}
		
		function sendId(id, tabla){
			//alert(id);
			cadena = "linkBorrar";
			if(tabla == "marcas" || tabla == "p")
				cadena = "linkBorrar2"
			document.getElementById(cadena).href= "../php/deletes.php?id=" + id + "&tabla=" + tabla;
		}
		

		function validar(e) {
			tecla=(document.all) ? e.keyCode : e.which;
			if(( (tecla<48 || tecla>57) && tecla !=46) && (tecla>31) && (tecla!=127)){
				$flag =  false;
				return $flag;
			}
		}
		
		function deleteAllConfirmation(x) {
			var r = confirm("¿Estás seguro que deseas eliminar varios elementos de: \"" +  x + "\"?");
			return r;
		}

		function mostrarModal(id){
			var modalC = "#modal";
			if(id != ""){
				modalC = modalC + id;
				$(modalC).modal('show')
			}
		}

//UBICACIÓN ACTUAL GOOGLE MAPS
//<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
var x=document.getElementById("demo");
var lat2;
var lon;

//Obtener una dirección de una latitud y longitud
function geocodeLatLng(idElement) {
 var geocoder = new google.maps.Geocoder;
  var latlng = {lat: lat2, lng: lon};
  geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        document.getElementById(idElement).value = results[1].formatted_address;
      } else {
        alert('No results found');
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });
}

function getLocation(){
	if (navigator.geolocation){
    	navigator.geolocation.getCurrentPosition(showPosition,showError);
    	
   	}
  	else{
  		alert("Geolocation is not supported by this browser.");
  	}
}

function showPosition(position)
  {
  lat2=position.coords.latitude;
  lon=position.coords.longitude;
  
  window.geocodeLatLng('direccion');

  latlon=new google.maps.LatLng(lat2, lon);
  mapholder=document.getElementById('mapholder');
  mapholder.style.height='250px';
  mapholder.style.width='100%';

  var myOptions={
  center:latlon,zoom:14,
  mapTypeId:google.maps.MapTypeId.ROADMAP,
  mapTypeControl:false,
  navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
  };
  var map=new google.maps.Map(document.getElementById("mapholder"),myOptions);
  var marker=new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
  }

function showError(error)
  {
  switch(error.code) 
    {
    case error.PERMISSION_DENIED:
      x.innerHTML="Haz desactivado tu localización correctamente."
      break;
    case error.POSITION_UNAVAILABLE:
      x.innerHTML="Tu ubicación no se encuentra disponible."
      break;
    case error.TIMEOUT:
      x.innerHTML="La solicitud para obtener la ubicación ha tardado demasiado, intenta de nuevo."
      break;
    case error.UNKNOWN_ERROR:
      x.innerHTML="Un error desconocido ocurrió."
      break;
    }
 }

 function codeAddress() {
  var address = document.getElementById('direccion').value;
  var geocoder = new google.maps.Geocoder();
  var mapOptions = {
    zoom: 15
  }
  mapholder=document.getElementById('mapholder');
  mapholder.style.height='250px';
  mapholder.style.width='100%';
  
  var map = new google.maps.Map(document.getElementById('mapholder'), mapOptions);

  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      alert('No se ha podido recuperar tu ubicación actual, ¡intenta de nuevo!\nStatus: ' + status);
    }
  });
}