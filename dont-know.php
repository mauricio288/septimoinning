<?php
    $id = $_GET["id"];
    $c = "";
    if(isset($_GET["c"]))
        $c = $_GET["c"];
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Septimoinning</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script> -->
        
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css"> 
        <link rel="stylesheet" href="assets/css/custom.css">

        <script src="https://use.fontawesome.com/e640964938.js"></script>
    </head>

    <body id="ReadApp">

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php include 'header.html' ?>


        <section ng-controller="cosasCtrl" ng-init="setID()"> <!-- Start section for 15 thongs -->
            <div class="container">
                <div class="news-section">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <h3 class="info-title">Las 15 cosas que no sabias de</h3>
                        </div>
                        <div class="col-md-12 info-container" ng-repeat="x in noSabias" ng-if="x.id == idSabias">
                             <div class="col-md-2 player-photo">
                                <img src="php/visualizar_archivo.php?id={{idSabias}}&tabla=cosas" alt="" class="img-responsive">
                                
                                 <div class="player-name">
                                    {{x.nombre}}
                                </div>
                            </div>
                            <div class="col-md-10 info-box" ng-repeat="c in CURIOSIDADArray | filter:curiosidadFilter  " >
                                <div class="col-md-12">
                                    <h3>{{c.nombre}}</h3>
                                </div>
                                <div class="col-md-2">
                                    <img src="php/visualizar_archivo.php?id={{c.id}}&tabla=cosas" alt="" class="img-responsive">
                                </div>
                                <div class="col-md-10">
                                <p style="text-align: justify;">{{c.texto}} </p>
                                </div>
                                <!--<div class="button know-btn">
                                    <a href=""><input class="yellow-btn" type="button" value="Siguiente "></a>
                                </div>-->
                            </div>

                            <div class="col-md-12 details-explorer">
                                <div class="col-md-1" ng-repeat="c in elinfiltrao = (CURIOSIDADArray | filter: buscador) ">
                                    <a ng-click="asdf(c)"><input class="number-btn" type="button" value="{{$index + 1}}"></a>
                                </div>
                            </div>    
                        </div>
                    </div>
                <div class="row">
                        <div class="col-md-12 spot-horizontal">
                            <h3 class="title-black">PUBLICIDAD</h3>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12 spot-horizontal">
                            <center><h3 class="title-black">Otras Curiosidades</h3></center>
                        </div>
                    <a ng-repeat="c in noSabias | limitTo:6 | orderBy:random"  href="dont-know.php?c={{idCuriosidad}}&id={{c.id}}">
                        <div class="col-md-2 info-container">
                                <img src="php/visualizar_archivo.php?id={{c.id}}&tabla=sabias" alt="" class="img-responsive">
                                <center><h3>{{c.nombre}}</h3></center>
                        </div>
                    </a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="disqus_thread"></div>
                            <script>

                            /**
                            *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                            *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                            /*
                            var disqus_config = function () {
                            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
                            */
                            (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');
                            s.src = 'https://http-septimoinning-com-web2016.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                            })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>
                </div>
            </div> 
            </div>
        </section>
   
        <?php include 'footer.html'; ?>

        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <!-- <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script> -->

        <script src="https://www.google-analytics.com/analytics.js" async defer></script>

        <script type="text/javascript">
            var get = { "id" : <?php echo $id ?> };
        </script>
        <script type="text/javascript">
            var curiosidad = { "id" : <?php echo $c ?> };
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
        <script type="text/javascript" src="js/angular.min.js"></script>
        <script src='js/rich_text/textAngular-rangy.min.js'></script>
        <script src='js/rich_text/textAngular-sanitize.min.js'></script>
        <script src='js/rich_text/textAngular.min.js'></script>
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>