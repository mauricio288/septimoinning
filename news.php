<?php
    
    $c = $_GET["c"];
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Septimoinning</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script> -->
        
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> 
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css"> 
        <link rel="stylesheet" href="assets/css/custom.css">

        <script src="https://use.fontawesome.com/e640964938.js"></script>
        <script type="text/javascript">
            var categoria = { "id" : <?php echo $c ?> };
        </script>
    </head>
    <body id="ReadApp">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <?php include 'header.html' ?>

        <section ng-controller="newsCtrl"> <!-- Start section for news -->
        	<div class="container">
        		<div class="row"> <!-- Row for filters and publicity, not visible in mobile -->

		        	<div class="col-md-2 filter-bar list-group">
		        		<h2>{{nombreCategoria.nombre}}</h2>
		        		<br>
		        		<div ng-repeat="c in categoriasArray">
		        			<a ng-click="asdf(c.id)" class="list-group-item">{{c.nombre}}</a>
	        			</div>
		        	</div>
		        	<div class="col-md-10" ng-if="nombreCategoria.nombre != 'Las 15 cosas de'"> <!-- Start news resumies -->
		        		<div class="col-md-6" ng-repeat="n in newsArray | filter:newGet">
							<!-- News here are nouns -->
		        			<div class="news-box">
                                <img ng-if="!n.idArchivo || n.idArchivo==0" class="img-responsive all-news-image" src="php/visualizar_archivo.php?tabla=noticias&id={{n.id}}" alt="" >
                                <img ng-if="n.idArchivo" class="img-responsive all-news-image" src="php/visualizar_archivo.php?id={{n.idArchivo}}" alt="" >
	        					<div class="news-titles">
	        						<h2>{{n.nombre}}</h2>
	        					</div>
	        					<div class="news-resume">
	        						<div ng-bind-html="preview(n.contenido) | limitTo: 250"></div>
	        					</div>
	        					<div class="button">
	        						<a href="news-single.php?n={{n.id}}&c={{nombreCategoria.id}}"><input class="yellow-btn" type="button" value="Leer más"></a>
	        					</div>
		        			</div>
		        		</div>
		        	</div>
                    
	        	</div>
        	</div>
        </section>

		<?php include 'footer.html'; ?>

	 	<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <!-- <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script> -->
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
        <script type="text/javascript" src="js/angular.min.js"></script>
        <script src='js/rich_text/textAngular-rangy.min.js'></script>
        <script src='js/rich_text/textAngular-sanitize.min.js'></script>
        <script src='js/rich_text/textAngular.min.js'></script>


        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>